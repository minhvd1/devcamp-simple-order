import { useState } from "react";
import { Col, Row, Button } from "reactstrap";

const OrderDetail = ({nameProp, priceProp, addTotalProp}) => {
    const [quantity, setQuantity] = useState(0);

    const addProduct = () => {
        setQuantity(quantity + 1);

        addTotalProp(priceProp);
    }

    return (
        <Col className="col-4 p-5">
            <Row className="border p-5">
                <Col className="col-12">
                    <b>
                        {nameProp}
                    </b>
                </Col>
                <Col className="col-12 mt-3">
                    Price: {priceProp} USD
                </Col>
                <Col className="col-12 mt-3">
                    Quantity: {quantity}
                </Col>
                <Col className="col-12 mt-3">
                    <Button
                        color="success"
                        onClick={addProduct}
                    >
                        Buy
                    </Button>
                </Col>
            </Row>
        </Col>
    )
}

export default OrderDetail;